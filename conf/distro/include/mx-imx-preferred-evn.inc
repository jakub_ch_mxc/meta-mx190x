# Use i.MX Kernel, U-Boot providers

PREFERRED_PROVIDER_u-boot_mx6ul = "u-boot-mys6ulx"
PREFERRED_PROVIDER_virtual/bootloader_mx6ul = "u-boot-mys6ulx"
PREFERRED_PROVIDER_virtual/kernel_mx6ul = "linux-mys6ulx"

# Default toolchains used in testing i.MX BSPs
DEFAULTTUNE_mx6ul = "cortexa7hf-neon"

# Enable the kenrel loadable module as default
#USE_GPU_VIV_MODULE = "1"

PREFERRED_VERSION_linux-libc-headers = "4.1"

# Use systemd as default init manager - comment out for now
#VIRTUAL-RUNTIME_init_manager = "systemd"
#PREFERRED_PROVIDER_udev = "systemd"
#PREFERRED_PROVIDER_udev-utils = "systemd"
#DISTRO_FEATURES_BACKFILL_CONSIDERED = "sysvinit"
#IMX_DEFAULT_DISTRO_FEATURES += " systemd"

GCCVERSION = "5.3.0"
