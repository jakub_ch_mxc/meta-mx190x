SUMMARY = "GPS test utilities"
SECTION = "drivers"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://setup.sh \
"

S = "${WORKDIR}"

do_install (){
    install -d ${D}/home/root/gps
    cp -rfv setup.sh ${D}/home/root/gps/
}


FILES_${PN} = "/home/root/gps/"