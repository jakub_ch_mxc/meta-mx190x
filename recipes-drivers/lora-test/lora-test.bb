SUMMARY = "Lora test utilities compiled from sx1302 hal"
SECTION = "drivers"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
#TODO: figure out correct licensing

SRC_URI = " \
    file://reset_lgw.sh \
    file://test_loragw_hal_rx \
    file://test_loragw_hal_tx \
"

S = "${WORKDIR}"

do_install (){
    install -d ${D}/home/root/lora
    install -m 0755 test_loragw_hal_rx ${D}/home/root/lora/
    install -m 0755 test_loragw_hal_tx ${D}/home/root/lora/
    install -m 0755 reset_lgw.sh ${D}/home/root/lora/
}

FILES_${PN} = "/home/root/lora/"
