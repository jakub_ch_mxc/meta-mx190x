SUMMARY = "MX190x dev starting point"
SECTION = "drivers"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://mxradio.c"

S = "${WORKDIR}"

do_compile() {
	     ${CXX} -std=c++11 mxradio.c -o mxradio
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 mxradio ${D}${bindir}
}
