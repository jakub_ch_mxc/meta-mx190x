#include <iostream>
#include <cstdint>
#include <unistd.h>

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

#define ARRAY_SIZE(array) sizeof(array)/sizeof(array[0])

#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

int main(int argc, char **argv)
{
  std::cout << "Hello MatchX!\n";
  char rd_buf[10] = {0,0,0,0,0,0,0,0,0};
  char wr_buf[10] = {1,2,3,4,5,6,7,8,9};

  int fd;

  if(argv[2][0] == 'm'){
    fd = open("/dev/spidev1.0", O_RDWR);
    if (fd<=0) {
      printf("Device not found\n");
      exit(1);
    }
    printf("master\n");
  }else{
    fd = open("/dev/spidev1.1", O_RDWR);
    if (fd<=0) {
      printf("Device not found\n");
      exit(1);
    }
    printf("slave!\n");
  }

  printf("Device found\n");

  if(argv[1][0] == 'r'){
    std::cout << "Reading!\n";
    while(1){
      if (read(fd, rd_buf, ARRAY_SIZE(rd_buf)) != ARRAY_SIZE(rd_buf))
        perror("Read Error");

      for(int i = 0; i < ARRAY_SIZE(rd_buf); i++)
        printf("%u ", rd_buf[i]);
      std::cout << "\n";
      sleep(1);
    }

  }else if(argv[1][0] == 'w'){
    std::cout << "Writing!\n";
    if (write(fd, wr_buf, ARRAY_SIZE(wr_buf)) != ARRAY_SIZE(wr_buf))
      perror("Write Error");
  }

  close(fd);

  return 0;
}












#define LED_G "22"
#define LED_B "26"
#define LED_R "27"

#define GPIO "/sys/class/gpio/"

uint32_t mask_r = 4;
uint32_t mask_g = 2;
uint32_t mask_b = 1;

void test_led()
{
  system("echo " LED_R " > " GPIO "export");
  system("echo " LED_G " > " GPIO "export");
  system("echo " LED_B " > " GPIO "export");

  system("echo out > " GPIO "gpio" LED_R "/direction");
  system("echo out > " GPIO "gpio" LED_G "/direction");
  system("echo out > " GPIO "gpio" LED_B "/direction");

  uint32_t count = 0;
  while(true){
    if(count & mask_r){
      system("echo 1 > " GPIO "gpio" LED_R "/value");
    }else{
      system("echo 0 > " GPIO "gpio" LED_R "/value");
    }

    if(count & mask_g){
      system("echo 1 > " GPIO "gpio" LED_G "/value");
    }else{
      system("echo 0 > " GPIO "gpio" LED_G "/value");
    }

    if(count & mask_b){
      system("echo 1 > " GPIO "gpio" LED_B "/value");
    }else{
      system("echo 0 > " GPIO "gpio" LED_B "/value");
    }

    count++;

    sleep(1);
  }
}
