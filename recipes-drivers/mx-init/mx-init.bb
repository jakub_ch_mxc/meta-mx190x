SUMMARY = "Linux init scripts"
SECTION = "drivers"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://mx-init.sh"
S = "${WORKDIR}"

inherit update-rc.d

INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME = "mx-init.sh"

# install it in the correct location for update-rc.d
do_install() {
  install -d ${D}${INIT_D_DIR}
  install -m 0755 mx-init.sh ${D}${INIT_D_DIR}/
}

FILES_${PN} = "${INIT_D_DIR}/mx-init.sh"