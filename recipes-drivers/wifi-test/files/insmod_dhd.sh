ap_mode=false
mfg_mode=false

while [ "$1" != "" ]; do
	case $1 in
		-ap   ) ap_mode=true;;
	        -mfg  ) mfg_mode=true;;
	esac
	shift
done

KVER=$(uname -a | awk '{ printf $3 }')
DHD=/lib/modules/$KVER/kernel/drivers/net/wireless/bcmdhd/bcmdhd.ko
FW=/lib/firmware/bcm/fw_bcmdhd.bin

if [ $ap_mode == "true" ]; then
	FW=/lib/firmware/bcm/fw_bcmdhd_apsta.bin
fi

if [ $mfg_mode == "true" ]; then
	FW=/lib/firmware/bcm/fw_bcmdhd_mfgtest.bin
fi

if [ ! -e $FW ]; then
	echo "ERROR: $FW doesn't exist"
	exit
fi

NVRAM=/lib/firmware/bcm/bcmdhd.cal

echo "FW=$FW"
echo "NVRAM=$NVRAM"
insmod $DHD firmware_path=$FW nvram_path=$NVRAM dhd_msg_level=0x1
