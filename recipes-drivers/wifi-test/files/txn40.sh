#!/bin/sh
echo "#*************************************************"
echo "# By Universal Scientific Industrial."
echo "# Script usage:" 
echo "# $0 <channel #> <mcs index> <Tx power in dbm units>" 
echo "#"
echo "# Example: test HT40 TX with channel 3, mcs 7, 5db"
echo "# $0 3 7 5"
echo "# Note: channel value=3~11"
echo "# Note: mcs value range=0~7"
echo "#*************************************************"

echo "./wlarm down"
./wlarm down
echo "./wlarm country ALL"
./wlarm country ALL
echo "./wlarm ibss_gmode -1"
./wlarm ibss_gmode -1
echo "./wlarm wsec 0"
./wlarm wsec 0
#echo "./wlarm legacylink 1"
#./wlarm legacylink 1
echo "./wlarm mpc 0"
./wlarm mpc 0
echo "./wlarm PM 0"
./wlarm PM 0
echo "./wlarm up"
./wlarm up
echo "./wlarm frameburst 1"
./wlarm frameburst 1
echo "./wlarm mimo_ss_stf 0"
./wlarm mimo_ss_stf 0

#./wlarm band $1
echo "./wlarm band b"
./wlarm band b
echo "./wlarm down"
./wlarm down
echo "./wlarm scansuppress 1"
./wlarm scansuppress 1
echo "./wlarm frameburst 1"
./wlarm frameburst 1
#./wlarm mimo_txbw $7
echo "./wlarm mimo_txbw 4"
./wlarm mimo_txbw 4
echo "./wlarm mimo_bw_cap 1"
./wlarm mimo_bw_cap 1
echo "./wlarm chanspec -c $2 -b $3 -w 40 -s -1"
./wlarm chanspec -c $1 -b 2 -w 40 -s -1
#echo "./wlarm chanspec $1 b l"
#./wlarm chanspec $1 b l
echo "./wlarm up"
./wlarm up
echo "./wlarm rate -1"
./wlarm rate -1
#./wlarm nrate -$4 $5 -s 0
echo "./wlarm nrate -m $2 -s 0"
./wlarm nrate -m $2 -s 0
echo "./wlarm txpwr1 -o -d $3"
./wlarm txpwr1 -o -d $3
echo "./wlarm txant 0"
./wlarm txant 0
echo "./wlarm antdiv 0"
./wlarm antdiv 0
echo "./wlarm pkteng_start 00:11:22:33:44:55 tx 100 1500 0"
./wlarm pkteng_start 00:11:22:33:44:55 tx 100 1500 0
echo "Script end."
