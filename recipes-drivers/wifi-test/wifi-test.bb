SUMMARY = "WiFi test utilities"
SECTION = "drivers"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
#TODO: figure out correct licensing

SRC_URI = " \
    file://bcmdhd.cal \
    file://fw_bcmdhd.bin \
    file://fw_bcmdhd_apsta.bin \
    file://fw_bcmdhd_mfgtest.bin \
    file://wl \
    file://wlarm \
    file://insmod_dhd.sh \
    file://rx.sh \
    file://rxn.sh \
    file://txbg.sh \
    file://txn.sh \
    file://txunmod.sh \
    file://stop.sh \
"

S = "${WORKDIR}"

do_install (){
    install -d ${D}${base_libdir}/firmware/bcm/
    cp -rfv bcmdhd.cal ${D}${base_libdir}/firmware/bcm/
    cp -rfv fw_bcmdhd.bin ${D}${base_libdir}/firmware/bcm/
    cp -rfv fw_bcmdhd_apsta.bin ${D}${base_libdir}/firmware/bcm/
    cp -rfv fw_bcmdhd_mfgtest.bin ${D}${base_libdir}/firmware/bcm/

    install -d ${D}${bindir}
    install -m 0755 wl ${D}${bindir}
    install -m 0755 wlarm ${D}${bindir}

    install -d ${D}/home/root/wifi
    cp -rfv insmod_dhd.sh ${D}/home/root/wifi/
    cp -rfv rx.sh ${D}/home/root/wifi/
    cp -rfv rxn.sh ${D}/home/root/wifi/
    cp -rfv txbg.sh ${D}/home/root/wifi/
    cp -rfv txn.sh ${D}/home/root/wifi/
    cp -rfv txunmod.sh ${D}/home/root/wifi/
    cp -rfv stop.sh ${D}/home/root/wifi/
    #workaround
    cp -rfv wl ${D}/home/root/wifi/wlarm
}


FILES_${PN} = "${base_libdir}/firmware/bcm \
            /home/root/wifi/ \
            ${bindir} \
"