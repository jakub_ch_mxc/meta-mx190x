IMAGE_INSTALL += "imx-kobs \
    tslib-calibrate \
    tslib-conf \
    tslib-tests \
    bzip2 \
    gzip \
    canutils \
    dosfstools \
    mtd-utils \
    mtd-utils-ubifs \
    ntpdate \
    vlan \
    tar \
    net-tools \
    ethtool \
    evtest \
    i2c-tools \
    iperf3 \
    iproute2 \
    iputils \
    udev-extraconf \
    rpm \
    iperf \
    openssh \
    openssl \
    v4l-utils \
    hostapd \
    mxradio \
    devmem2 \
    gpsd \
    gps-utils \
    wifi-test \
    lora-test \
    gps-test \
    mx-init"
